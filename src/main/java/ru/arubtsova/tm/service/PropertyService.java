package ru.arubtsova.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.api.service.IPropertyService;

import java.io.InputStream;
import java.util.Properties;

public class PropertyService implements IPropertyService {

    @NotNull
    private static final String VALUE_DEFAULT_STRING = "";

    @NotNull
    private static final String VALUE_DEFAULT_INTEGER = "0";

    @NotNull
    private static final String FILE_NAME = "application.properties";

    @NotNull
    private static final String PASSWORD_SECRET_KEY_JAVA_OPTS = "password.secret";

    @NotNull
    private static final String PASSWORD_SECRET_KEY_ENVIRONMENT = "PASSWORD_SECRET";

    @NotNull
    private static final String PASSWORD_SECRET_DEFAULT = "";

    @NotNull
    private static final String PASSWORD_ITERATION_KEY_JAVA_OPTS = "password.iteration";

    @NotNull
    private static final String PASSWORD_ITERATION_KEY_ENVIRONMENT = "PASSWORD_ITERATION";

    @NotNull
    private static final Integer PASSWORD_ITERATION_DEFAULT = 10111001;

    @NotNull
    private static final String APPLICATION_VERSION_KEY_JAVA_OPTS = "application.version";

    @NotNull
    private static final String APPLICATION_VERSION_KEY_ENVIRONMENT = "APPLICATION_VERSION";

    @NotNull
    private static final String APPLICATION_VERSION_DEFAULT = "";

    @NotNull
    private static final String APPLICATION_DEVELOPER_KEY_JAVA_OPTS = "developer.name";

    @NotNull
    private static final String APPLICATION_DEVELOPER_KEY_ENVIRONMENT = "DEVELOPER_NAME";

    @NotNull
    private static final String APPLICATION_DEVELOPER_DEFAULT = "Anastasia Rubtsova";

    @NotNull
    private static final String DEVELOPER_EMAIL_KEY_JAVA_OPTS = "developer.email";

    @NotNull
    private static final String DEVELOPER_EMAIL_KEY_ENVIRONMENT = "DEVELOPER_EMAIL";

    @NotNull
    private static final String DEVELOPER_EMAIL_DEFAULT = "Lafontana@mail.ru";

    @NotNull
    private static final String DEVELOPER_COMPANY_KEY_JAVA_OPTS = "developer.company";

    @NotNull
    private static final String DEVELOPER_COMPANY_KEY_ENVIRONMENT = "DEVELOPER_COMPANY";

    @NotNull
    private static final String DEVELOPER_COMPANY_DEFAULT = "TSC";

    @NotNull
    private final Properties properties = new Properties();

    @NotNull
    @Override
    public String getValue(
            @Nullable final String javaOpts,
            @Nullable final String environment,
            @Nullable final String defaultValue
    ) {
        return getValueString(javaOpts, environment, defaultValue);
    }

    @NotNull
    @Override
    public String getValueString(
            @Nullable final String javaOpts,
            @Nullable final String environment,
            @Nullable final String defaultValue
    ) {
        if (javaOpts == null || javaOpts.isEmpty()) return VALUE_DEFAULT_STRING;
        if (environment == null || environment.isEmpty()) return VALUE_DEFAULT_STRING;
        if (defaultValue == null) return VALUE_DEFAULT_STRING;
        if (System.getProperties().containsKey(javaOpts))
            return System.getProperty(javaOpts);
        if (System.getenv().containsKey(environment))
            return System.getenv(environment);
        return properties.getProperty(javaOpts, defaultValue);
    }

    @NotNull
    @Override
    public Integer getValueInteger(
            @Nullable final String javaOpts,
            @Nullable final String environment,
            @Nullable final Integer defaultValue
    ) {
        @NotNull final String value = getValue(javaOpts, environment, VALUE_DEFAULT_INTEGER);
        if (value.isEmpty())
            if (defaultValue == null) return Integer.parseInt(VALUE_DEFAULT_INTEGER);
        return Integer.parseInt(value);
    }

    @SneakyThrows
    public PropertyService() {
        @Nullable final InputStream inputStream = ClassLoader.getSystemResourceAsStream(FILE_NAME);
        if (inputStream == null) return;
        properties.load(inputStream);
        inputStream.close();
    }

    @NotNull
    @Override
    public String getPasswordSecret() {
        return getValueString(
                PASSWORD_SECRET_KEY_JAVA_OPTS,
                PASSWORD_SECRET_KEY_ENVIRONMENT,
                PASSWORD_SECRET_DEFAULT
        );
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        return getValueInteger(
                PASSWORD_ITERATION_KEY_JAVA_OPTS,
                PASSWORD_ITERATION_KEY_ENVIRONMENT,
                PASSWORD_ITERATION_DEFAULT
        );
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        return getValueString(
                APPLICATION_VERSION_KEY_JAVA_OPTS,
                APPLICATION_VERSION_KEY_ENVIRONMENT,
                APPLICATION_VERSION_DEFAULT
        );
    }

    @NotNull
    @Override
    public String getDeveloperName() {
        return getValueString(
                APPLICATION_DEVELOPER_KEY_JAVA_OPTS,
                APPLICATION_DEVELOPER_KEY_ENVIRONMENT,
                APPLICATION_DEVELOPER_DEFAULT
        );
    }

    @NotNull
    @Override
    public String getDeveloperEmail() {
        return getValueString(
                DEVELOPER_EMAIL_KEY_JAVA_OPTS,
                DEVELOPER_EMAIL_KEY_ENVIRONMENT,
                DEVELOPER_EMAIL_DEFAULT
        );
    }

    @NotNull
    @Override
    public String getDeveloperCompany() {
        return getValueString(
                DEVELOPER_COMPANY_KEY_JAVA_OPTS,
                DEVELOPER_COMPANY_KEY_ENVIRONMENT,
                DEVELOPER_COMPANY_DEFAULT
        );
    }

}
