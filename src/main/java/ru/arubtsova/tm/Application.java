package ru.arubtsova.tm;

import org.jetbrains.annotations.NotNull;
import ru.arubtsova.tm.bootstrap.Bootstrap;
import ru.arubtsova.tm.util.SystemUtil;

public class Application {

    public static void main(final String[] args) {
        System.out.println(SystemUtil.getPID());
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}
