package ru.arubtsova.tm.exception.entity;

import ru.arubtsova.tm.exception.AbstractException;

public class UserNotFoundException extends AbstractException {

    public UserNotFoundException() {
        super("Error! User not found...");
    }

}
