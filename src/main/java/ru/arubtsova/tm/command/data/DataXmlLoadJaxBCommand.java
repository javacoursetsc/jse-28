package ru.arubtsova.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.command.AbstractDataCommand;
import ru.arubtsova.tm.dto.Domain;
import ru.arubtsova.tm.enumerated.Role;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class DataXmlLoadJaxBCommand extends AbstractDataCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-xml-load-jaxb";
    }

    @NotNull
    @Override
    public String description() {
        return "load xml data from file by JaxB library.";
    }

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("Data xml load by JaxB:");
        @NotNull final File file = new File(FILE_XML_JAXB);
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(file);
        setDomain(domain);
        System.out.println("Successful");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{
                Role.ADMIN
        };
    }

}
