package ru.arubtsova.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.command.AbstractDataCommand;
import ru.arubtsova.tm.dto.Domain;
import ru.arubtsova.tm.enumerated.Role;

import java.io.FileOutputStream;

public class DataJsonSaveFasterXmlCommand extends AbstractDataCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-json-save-fasterxml";
    }

    @NotNull
    @Override
    public String description() {
        return "save json data to file.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("Data json save:");
        @NotNull final Domain domain = getDomain();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(FILE_JSON_FASTERXML);
        fileOutputStream.write(json.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
        System.out.println("Successful");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{
                Role.ADMIN
        };
    }

}
