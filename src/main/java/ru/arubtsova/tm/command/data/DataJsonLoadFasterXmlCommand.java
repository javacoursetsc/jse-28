package ru.arubtsova.tm.command.data;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.command.AbstractDataCommand;
import ru.arubtsova.tm.dto.Domain;
import ru.arubtsova.tm.enumerated.Role;

import java.nio.file.Files;
import java.nio.file.Paths;

public class DataJsonLoadFasterXmlCommand extends AbstractDataCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-json-load-fasterxml";
    }

    @NotNull
    @Override
    public String description() {
        return "load json data from file.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("Data json load:");
        @NotNull final String json = new String(Files.readAllBytes(Paths.get(FILE_JSON_FASTERXML)));
        @NotNull final ObjectMapper objectMapper = new ObjectMapper(new JsonFactory());
        @NotNull final Domain domain = objectMapper.readValue(json, Domain.class);
        setDomain(domain);
        System.out.println("Successful");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{
                Role.ADMIN
        };
    }

}
