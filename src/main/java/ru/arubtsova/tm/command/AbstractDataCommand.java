package ru.arubtsova.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.dto.Domain;

public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    protected static final String FILE_BASE64 = "./data.base64";

    @NotNull
    protected static final String FILE_BINARY = "./data.bin";

    @NotNull
    protected static final String FILE_JSON_FASTERXML = "./data-fasterxml.json";

    @NotNull
    protected static final String FILE_JSON_JAXB = "./data-jaxb.json";

    @NotNull
    protected static final String FILE_XML_FASTERXML = "./data-fasterxml.xml";

    @NotNull
    protected static final String FILE_XML_JAXB = "./data-jaxb.xml";

    @NotNull
    protected static final String FILE_YAML_FASTERXML = "./data-fasterxml.yaml";

    @NotNull
    protected static final String JAVAX_XML_BIND_CONTEXT_FACTORY = "javax.xml.bind.context.factory";

    @NotNull
    protected static final String ORG_ECLIPSE_PERSISTENCE_JAXB_JAXBCONTEXT_FACTORY = "org.eclipse.persistence.jaxb.JAXBContextFactory";

    @NotNull
    protected static final String ECLIPSELINK_MEDIA_TYPE = "eclipselink.media-type";

    @NotNull
    protected static final String APPLICATION_JSON = "application/json";

    @NotNull
    public Domain getDomain() {
        @NotNull final Domain domain = new Domain();
        domain.setProjects(serviceLocator.getProjectService().findAll());
        domain.setTasks(serviceLocator.getTaskService().findAll());
        domain.setUsers(serviceLocator.getUserService().findAll());
        return domain;
    }

    public void setDomain(@Nullable final Domain domain) {
        if (domain == null) return;
        serviceLocator.getUserService().clear();
        serviceLocator.getUserService().addAll(domain.getUsers());
        serviceLocator.getProjectService().clear();
        serviceLocator.getProjectService().addAll(domain.getProjects());
        serviceLocator.getTaskService().clear();
        serviceLocator.getTaskService().addAll(domain.getTasks());
        serviceLocator.getAuthService().logout();
        System.out.println("You've been logged out. Please, login in the system again...");
    }

}
