package ru.arubtsova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.command.AbstractProjectCommand;
import ru.arubtsova.tm.exception.entity.ProjectNotFoundException;
import ru.arubtsova.tm.model.Project;
import ru.arubtsova.tm.util.TerminalUtil;

import java.util.Optional;

public class ProjectFinishByIndexCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-finish-by-index";
    }

    @NotNull
    @Override
    public String description() {
        return "change project status to Complete by project index.";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("Project:");
        System.out.println("Enter Project Index:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final Optional<Project> project = serviceLocator.getProjectService().finishByIndex(userId, index);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
    }

}
