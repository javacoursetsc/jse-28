package ru.arubtsova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.command.AbstractProjectCommand;
import ru.arubtsova.tm.exception.entity.ProjectNotFoundException;
import ru.arubtsova.tm.model.Project;
import ru.arubtsova.tm.util.TerminalUtil;

import java.util.Optional;

public class ProjectRemoveByIndexCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-remove-by-index";
    }

    @NotNull
    @Override
    public String description() {
        return "delete a project by index.";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("Project Removal:");
        System.out.println("Enter Project Index:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @Nullable final Project project = serviceLocator.getProjectService().removeByIndex(userId, index);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        System.out.println("Project was successfully removed");
    }

}
