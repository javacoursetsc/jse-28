package ru.arubtsova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.api.IBusinessRepository;
import ru.arubtsova.tm.exception.entity.ObjectNotFoundException;
import ru.arubtsova.tm.model.AbstractBusinessEntity;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public abstract class AbstractBusinessRepository<E extends AbstractBusinessEntity> extends AbstractRepository<E> implements IBusinessRepository<E> {

    @NotNull
    public Predicate<E> predicateById(@NotNull final String id) {
        return e -> id.equals(e.getId());
    }

    @NotNull
    public Predicate<E> predicateByName(@NotNull final String name) {
        return e -> name.equals(e.getName());
    }

    @NotNull
    public Predicate<E> predicateByUserId(@NotNull final String userId) {
        return e -> userId.equals(e.getUserId());
    }

    @NotNull
    public List<E> findAll(@NotNull final String userId) {
        return map.values().stream()
                .filter(predicateByUserId(userId))
                .collect(Collectors.toList());
    }

    @NotNull
    public List<E> findAll(@NotNull final String userId, @NotNull final Comparator<E> comparator) {
        return map.values().stream()
                .filter(predicateByUserId(userId))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Nullable
    @Override
    public E add(@NotNull final String userId, @NotNull final E entity) {
        map.put(entity.getId(), entity);
        entity.setUserId(userId);
        return entity;
    }

    @NotNull
    @Override
    public Optional<E> findById(@NotNull final String userId, @NotNull final String id) {
        return map.values().stream()
                .filter(predicateByUserId(userId))
                .filter(predicateById(id))
                .findFirst();
    }

    @NotNull
    @Override
    public Optional<E> findByIndex(@NotNull final String userId, @NotNull final Integer index) {
        return map.values().stream()
                .filter(predicateByUserId(userId))
                .skip(index)
                .findFirst();
    }

    @NotNull
    @Override
    public Optional<E> findByName(@NotNull final String userId, @NotNull final String name) {
        return map.values().stream()
                .filter(predicateByUserId(userId))
                .filter(predicateByName(name))
                .limit(1)
                .findFirst();
    }

    @Override
    public void clear(@NotNull final String userId) {
        List<String> entityUserId = map.values().stream()
                .filter(predicateByUserId(userId))
                .map(E::getId)
                .collect(Collectors.toList());
        entityUserId.forEach(map::remove);
    }

    @Nullable
    @Override
    public E removeById(@NotNull final String userId, @NotNull final String id) {
        final Optional<E> entity = findById(userId, id);
        entity.ifPresent(this::remove);
        entity.orElseThrow(ObjectNotFoundException::new);
        return entity.orElse(null);
    }

    @Nullable
    @Override
    public E removeByIndex(@NotNull final String userId, @NotNull final Integer index) {
        final Optional<E> entity = findByIndex(userId, index);
        entity.ifPresent(this::remove);
        entity.orElseThrow(ObjectNotFoundException::new);
        return entity.orElse(null);
    }

    @Nullable
    @Override
    public E removeByName(@NotNull final String userId, @NotNull final String name) {
        final Optional<E> entity = findByName(userId, name);
        entity.ifPresent(this::remove);
        entity.orElseThrow(ObjectNotFoundException::new);
        return entity.orElse(null);
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final E entity) {
        map.remove(entity.getId());
    }

}
